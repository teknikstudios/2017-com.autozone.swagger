module.exports.Parse = function(str) {
  var cookie = {};
  var cookieKeyValues = str.split(';');
  cookieKeyValues.map(function(keyValueString) {
      var keyValue = keyValueString.trim().split('=');
      cookie[keyValue[0]] = keyValue[1];
  });
  return cookie;
}