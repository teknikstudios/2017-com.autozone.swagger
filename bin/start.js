#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app/app');
var http = require('http');
var https = require('https');
var fs = require('fs');
var compressor = require('node-minify');
var uglifyjs = require("uglify-es");
var appConfig = app.get('config');

// Using YUI Compressor for CSS
compressor.minify({
  compressor: 'clean-css',
  input: 'app/public/src/css/autozone-swagger/1.0.0/autozone-swagger.css',
  output: 'app/public/'+appConfig.env+'/css/autozone-swagger-1.0.0.css',
  callback: function(err, min){
    if (err) {
      console.log(err);
    }
  }
});

// Concatenate JS files into one file
var options = {};
if (appConfig.env != 'dist') {
  options.sourceMap = {
    filename: 'autozone-swagger-1.0.0.js',
    url: '/public/js/autozone-swagger-1.0.0.js.map'
  }
}

var readFile = function(filename) {
  var str = fs.readFileSync(filename).toString();

  return str.replace('<%= hostname %>', appConfig.hostname);
}
/*
var result = uglifyjs.minify({
  'autozone-swagger.js': readFile('app/public/src/js/autozone-swagger/1.0.0/autozone-swagger.js')
}, options);

if (result.error) {
  console.log(result);
}

fs.writeFileSync('app/public/'+appConfig.env+'/js/autozone-swagger-1.0.0.js', result.code);
if (appConfig.env != 'dist') {
  fs.writeFileSync('app/public/'+appConfig.env+'/js/autozone-swagger-1.0.0.js.map', result.map);
}
*/

/**
 * Get port from environment and store in Express.
 */

//var port = normalizePort(process.env.PORT || appConfig.port);
//var sslport = normalizePort(process.env.PORT || appConfig.sslPort);
//app.set('port', port);
//app.set('sslport', sslport);
//app.set('hostname', appConfig.hostname);

/**
 * Create HTTP server.
 */

 var options = {
     key: fs.readFileSync(__dirname + '/server.key'),
     cert: fs.readFileSync(__dirname + '/server.crt'),
     requestCert: false,
     rejectUnauthorized: false
 };


/**
 * Listen on provided port, on all network interfaces.
 */
var server = null;
var port = appConfig.port;

if (appConfig.useSsl !== true) {
  server = http.createServer(app);
  if (appConfig.hostip) {
    server.listen(appConfig.port, appConfig.hostip);
  } else {
    server.listen(appConfig.port);
  }
  server.on('error', onError);
  server.on('listening', onListening);
} else if (appConfig.useSsl === true) {
  server = https.createServer(options, app);
  if (appConfig.hostip) {
    server.listen(appConfig.port, appConfig.hostip);
  } else {
    server.listen(appConfig.port);
  }
  server.on('error', onError);
  server.on('listening', onListening);
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  // debug('Listening on ' + bind);
}
