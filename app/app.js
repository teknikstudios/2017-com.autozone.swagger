var express = require('express');
var sessions = require('client-sessions');
var cookieParser = require('cookie-parser');
var url = require('url');

if (process.argv.indexOf('--local') != -1) {
  var appConfig = require('./config-local.json')
} else if (process.argv.indexOf('--dev') != -1) {
  var appConfig = require('./config-dev.json')
} else {
  var appConfig = require('./config-dist.json');
}

var app = express();
app.enable('strict routing');
app.set('config', appConfig);
//app.set('hostname', appConfig.hostname);
//app.set('hostip', appConfig.hostip);

// Create the router using the same routing options as the app.
var router = express.Router({
    caseSensitive: app.get('case sensitive routing'),
    strict       : app.get('strict routing')
});

app.locals.basedir = __dirname;

app.use(cookieParser());

app.use(sessions({
  cookieName: 'session', // cookie name
  secret: 'ua{2wT>gEh]{E!d<+ptN$MQ-uT-y#A+e}E2?ZZ3MYkQ%=v#z,V-#ryzYn9t^HkX~',
  duration: appConfig.cookieDuration, // the duration that the session will stay valid in ms
  activeDuration: appConfig.cookieActiveDuration, // if expiresIn < activeDuration, the session will be extended by activeDuration
  cookie: {
    ephemeral: appConfig.cookieIsEphemeral, // cookie expires when the browser closes
    httpOnly: true, // cookie is not accessible from javascript
    secure: appConfig.useSslCookies // when true, cookie will only be sent over SSL. use key 'secureProxy' instead if you handle SSL not in your node process
  }
}));

// Authentication and Authorization Middleware
var auth = function(req, res, next) {
  if (req.session && req.session.authorized && req.session.user && req.session.pass) {
    next();
  } else {
    res.redirect('/login');
  }
};

// Authentication and Authorization Middleware
var apiAuth = function(req, res, next) {
  if (req.session && req.session.authorized && req.session.user && req.session.pass) {
    next();
  } else {
    res.status(403).send({ success: false, error: 'forbidden' })
  }
};


var nocache = function(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}

// view engine setup
app.set('views', __dirname + '/templates');
app.set('view engine', 'pug');

// uncomment after placing your favicon in /dist
//app.use(favicon(__dirname + '/dist/favicon.ico'));

app.use('/', require('./routes/home/index'));
app.use('/public', express.static(__dirname + '/public/'+appConfig.env));
app.use('/libraries', express.static(__dirname + '/public/lib'));
app.use('/images', express.static(__dirname + '/public/images'));
app.use('/swagger', auth, require('./routes/swagger/index'));
app.use('/swagger/viewer', auth, express.static( __dirname + '/public/lib/swagger-ui/3.4.0'));
app.use('/svn', apiAuth, require('./routes/svn/index'));
app.use('/validator', apiAuth, require('./routes/validator/index'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = (appConfig.env === 'dev') ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
