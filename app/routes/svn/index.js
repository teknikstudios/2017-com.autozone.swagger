var express = require('express');
var fs = require('fs-extra');
var bodyParser = require('body-parser');
var path = require('path');
var svn = require('node-svn-ultimate');
var app = express();

app.enable('strict routing');

var router = express.Router({
    caseSensitive: app.get('case sensitive routing'),
    strict       : app.get('strict routing')
});

router.use(bodyParser.text({limit:"5mb"}));

/*
 sanitizePath
*/
var sanitizePath = function(path) {
  path = decodeURIComponent(path);
  if (path.match(/[^-_ A-Za-z0-9.\/]/g)) {
    throw 'Invalid filename '+path+'. Only dash, underscore, space, A thru Z, 0 thru 9, period and forward slash are allowed.';
  }
  if (path.match(/\.\//g, '')) {
    throw 'Filename may not contain ./';
  }
  if (path.length > 255) {
    throw 'File path may not exceed 255 characters.';
  }
  return path;
}

/*
 sendError
*/
var sendError = function(res, err) {
  //console.log(err);
  res.status(500).send({ success: false, error: err })
  return false;
}

/*
 List a directory
*/
router.get(/^((\/)|(\/((snippets)|(specs)|(jsonschemas))\/(.*\/)?))$/, function(req, res, next) {

  svnCheckout(req, res, req.url, {containingDirectory:true}, (paths, workingCopyCleanupCallback) => {
    if (paths.isFile == true) {
      workingCopyCleanupCallback();
      return sendError(res, 'Directory expected');
    }

    svn.commands.list(paths.remotePath+encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass }, function(err, files) {
      var output = { success: true, directories: [], files: [] };

      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }

      if (files && files !== undefined && files.list !== undefined && files.list.entry !== undefined) {
        if (!Array.isArray(files.list.entry)) {
          files.list.entry = [files.list.entry];
        }

        for (var i=0; i<files.list.entry.length; i++) {
          var entry = files.list.entry[i];

          if (entry.$ === undefined || typeof entry.$ !== 'object') {
            continue;
          }

          if (entry.$.kind == 'dir') {
            output.directories.push(entry.name);
          } else if (entry.$.kind == 'file') {
            output.files.push( { name: entry.name, size: entry.size } );
          }
        }
      }

      workingCopyCleanupCallback();
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(output));
    });

  }, (err) => {
    sendError(res, err);
  });

});

/*
 Delete a directory
*/
router.delete(/^\/((snippets)|(specs))\/.*\/$/g, function(req, res, next) {

  svnCheckout(req, res, req.url, {containingDirectory:true}, (paths, workingCopyCleanupCallback) => {
    if (paths.isFile == true) {
      workingCopyCleanupCallback();
      return sendError(res, 'Directory expected');
    }

    // Delete the directory from the repo
    svn.commands.del(paths.remotePath+encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass, params: [ '-m "Deleted directory"' ] }, function( err ) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }

      workingCopyCleanupCallback();
      var output = { success: true }
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(output));
    });

  }, (err) => {
    sendError(res, err);
  });

});

/*
 Create a directory
*/
router.post(/^\/((snippets)|(specs))\/.*\/$/g, function(req, res, next) {

  svnCheckout(req, res, req.url, {containingDirectory:true}, (paths, workingCopyCleanupCallback) => {
    if (paths.isFile == true) {
      workingCopyCleanupCallback();
      return sendError(res, 'Directory expected');
    }

    // Create the directory on the repo
    svn.commands.mkdir(paths.remotePath+encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass, params: [ '-m "Created new directory"' ] }, function( err ) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }

      workingCopyCleanupCallback();
      var output = { success: true }
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(output));
    });

  }, (err) => {
    sendError(res, err);
  });
});

/*
 Update (rename) a directory
*/
router.put(/^\/((snippets)|(specs))\/.*\/$/g, function(req, res, next) {
  svnCheckout(req, res, req.url, {containingDirectory:true}, (paths, workingCopyCleanupCallback) => {
    if (paths.isFile == true) {
      workingCopyCleanupCallback();
      return sendError(res, 'Directory expected');
    }

    // See if the file exists in the repo
    svn.commands.info(paths.remotePath + encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass, force: true }, function( err, resp ) {
      if (err) {
        // The file isn't in the repo (perhaps someone deleted it out from under another user), so create one now and add it to the repo
        svn.commands.mkdir(paths.remotePath+encodeURIComponent(req.body), { username: req.session.user, password: req.session.pass, params: [ '-m "Created new directory"' ] }, function( err ) {
          if (err) {
            workingCopyCleanupCallback();
            return sendError(res, err);
          }

          workingCopyCleanupCallback();
          var output = { success: true }
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(output));
        });
      } else {
        // Rename the directory
        svn.commands.move(paths.remotePath+encodeURIComponent(paths.fileName), paths.remotePath+encodeURIComponent(req.body), { username: req.session.user, password: req.session.pass, params: [ '-m "Rename directory"' ] }, function( err ) {
          if (err) {
            workingCopyCleanupCallback();
            return sendError(res, err);
          }

          workingCopyCleanupCallback();
          var output = { success: true }
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(output));
        });
      }
    });


  }, (err) => {
    sendError(res, err);
  });
});



/*
 Get a file
*/
router.get(/^\/((snippets)|(specs)|(jsonschemas))\/.*$/, function(req, res, next) {

  svnCheckout(req, res, req.url, null, (paths, workingCopyCleanupCallback) => {
    if (paths.isFile == false) {
      workingCopyCleanupCallback();
      return sendError(res, 'Not a file');
    }

    let contents = ""
    try {
      contents = fs.readFileSync(paths.localPath, 'utf8');
    } catch (err) {
      return sendError(res, err);
    }
    workingCopyCleanupCallback();
    res.setHeader('Content-Type', 'text/plain');
    res.send(contents);
  }, (err) => {
    sendError(res, err);
  });

});


/*
 Delete a file
*/
router.delete(/^\/((snippets)|(specs))\/.+[^\/]$/, function(req, res, next) {

  svnCheckout(req, res, req.url, null, (paths, workingCopyCleanupCallback) => {
    if (paths.isFile == false) {
      workingCopyCleanupCallback();
      return sendError(res, 'File expected');
    }

    // Create the directory on the repo
    svn.commands.del(paths.remotePath, { username: req.session.user, password: req.session.pass, params: [ '-m "Created new directory"' ] }, function( err ) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }

      workingCopyCleanupCallback();
      var output = { success: true }
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(output));
    });

  }, (err) => {
    sendError(res, err);
  });

});


/*
 Create a file
*/
router.post(/^\/((snippets)|(specs))\/.+[^\/]$/, function(req, res, next) {
  // Do an update for the file to make sure it doesn't exist
  // Create the file on the filesystem
  // Add the file to SVN
  // Commit the file

  var fileExists = false; // true if the file exists in the repo
  var overwrite = (req.headers && req.headers.hasOwnProperty('x-action') && req.headers['x-action'] == 'overwrite') ? true : false;

  var writeFile = function(paths, workingCopyCleanupCallback) {
    fs.writeFile(paths.localPath + paths.fileName, req.body, function(err) {
      if (err) {
        return sendError(res, err);
      }

      if (fileExists) {
        commitFile(paths, workingCopyCleanupCallback);
      } else {
        addFile(paths, workingCopyCleanupCallback);
      }
    });
  }

  var addFile = function(paths, workingCopyCleanupCallback) {
    svn.commands.add([paths.localPathEscaped + paths.fileNameEscaped], { username: req.session.user, password: req.session.pass }, function(err) {
      if (err) {
        workingCopyCleanupCallback();
        sendError(res, err);
      } else {
        commitFile(paths, workingCopyCleanupCallback);
      }
    });
  }

  var commitFile = function(paths, workingCopyCleanupCallback) {
    var comments = (req.headers && req.headers.hasOwnProperty('x-comments')) ? req.headers['x-comments'] : '';
    if (!comments) {
      comments = '';
    }

    svn.commands.commit(paths.localPathEscaped + paths.fileNameEscaped, { username: req.session.user, password: req.session.pass, params: [ '-m "' + comments.replace(/"/g, '\"') + '"' ] }, function(err) {
      if (err) {
        workingCopyCleanupCallback();
        sendError(res, err);
      } else {
        workingCopyCleanupCallback();
        var output = { success: true }
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(output));
      }
    });
  }

  // Checkout the containing directory
  svnCheckout(req, res, req.url, {containingDirectory: true}, (paths, workingCopyCleanupCallback) => {
    if (!paths.isFile) {
      workingCopyCleanupCallback();
      return sendError(res, 'Not a file');
    }

    // See if the file exists in the repo
    svn.commands.info(paths.remotePath + encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass, force: true }, function( err, resp ) {
      if (err) {
        // The file isn't in the repo, so create one now and add it to the repo
        fileExists = false;
        writeFile(paths, workingCopyCleanupCallback);
      } else {
        // The file is in the repo, so we're overwriting it... if the overwrite flag isn't set, return an error
        if (!overwrite) {
          workingCopyCleanupCallback();
          var output = { success: false, errorCode: 409, errorMsg: 'Document already exists' }
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(output));
          return;
        }

        // Get the file from the remote
        svn.commands.update(paths.localPathEscaped + paths.fileNameEscaped, { username: req.session.user, password: req.session.pass, force: true }, function( err ) {
          if (err) {
            workingCopyCleanupCallback();
            return sendError(res, 'Unable to check out file for overwriting');
          } else {
            fileExists = true;
            writeFile(paths, workingCopyCleanupCallback);
          }
        });

      }
    });

  }, (err) => {
    sendError(res, err);
  });

});

/*
 Update a file's contents or rename a file
*/
router.put(/^\/((snippets)|(specs))\/.+[^\/]$/, function(req, res, next) {

  var success = function() {
    var output = { success: true }
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(output));
  }

  var renameFile = function(paths, workingCopyCleanupCallback) {
    svn.commands.move(paths.remotePath + encodeURIComponent(paths.fileName), paths.remotePath + encodeURIComponent(req.body), { username: req.session.user, password: req.session.pass, params: [ '-m "Rename file"' ] }, function( err ) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }
      workingCopyCleanupCallback();
      success();
    });
  }

  var addFile = function(paths, workingCopyCleanupCallback) {
    fs.writeFile(paths.localPath + paths.fileName, req.body, function(err) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }
      svn.commands.add([paths.localPathEscaped + paths.fileNameEscaped], { username: req.session.user, password: req.session.pass }, function(err) {
        if (err) {
          workingCopyCleanupCallback();
          return sendError(res, err);
        } else {
          commitFile(paths, workingCopyCleanupCallback);
        }
      });
    });
  }

  var updateFile = function(paths, workingCopyCleanupCallback) {
    fs.writeFile(paths.localPath + paths.fileName, req.body, function(err) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }
      commitFile(paths, workingCopyCleanupCallback);
    });
  }

  var commitFile = function(paths, workingCopyCleanupCallback) {
    var comments = (req.headers && req.headers.hasOwnProperty('x-comments')) ? req.headers['x-comments'] : '';
    if (!comments) {
      comments = '';
    }

    svn.commands.commit(paths.localPathEscaped + paths.fileNameEscaped, { username: req.session.user, password: req.session.pass, params: [ '-m "' + comments.replace(/"/g, '\"') + '"' ] }, function(err) {
      if (err) {
        workingCopyCleanupCallback();
        return sendError(res, err);
      }
      workingCopyCleanupCallback();
      success();
    });
  }

  var action = req.headers.hasOwnProperty('x-action') ? req.headers['x-action'] : 'update';
  switch (action) {
    case 'rename':
        svnCheckout(req, res, req.url, {containingDirectory:true}, (paths, workingCopyCleanupCallback) => {
          if (paths.isFile == false) {
            workingCopyCleanupCallback();
            return sendError(res, 'File expected');
          }
          // See if the file exists in the repo
          svn.commands.info(paths.remotePath + encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass, force: true }, function( err, resp ) {
            if (err) {
              // The file isn't in the repo (perhaps someone deleted it out from under another user), so create one now and add it to the repo
              paths.fileName = req.body;
              paths.fileNameEscaped = paths.fileName.replace(/ /g, '\\ ');
              addFile(paths, workingCopyCleanupCallback);
            } else {
              // Checkout the file and rename it
              svn.commands.update(paths.localPathEscaped + paths.fileNameEscaped, { username: req.session.user, password: req.session.pass, force: true }, function( err ) {
                if (err) {
                  clearWorkingCopyPath();
                  sendError(res, err);
                } else {
                  renameFile(paths, workingCopyCleanupCallback);
                }
              });
            }
          });
        }, (err) => {
          sendError(res, err);
        });
        break;

    case 'update':
        svnCheckout(req, res, req.url, {containingDirectory:true}, (paths, workingCopyCleanupCallback) => {
          if (paths.isFile == false) {
            workingCopyCleanupCallback();
            return sendError(res, 'File expected');
          }

          // See if the file exists in the repo
          svn.commands.info(paths.remotePath + encodeURIComponent(paths.fileName), { username: req.session.user, password: req.session.pass, force: true }, function( err, resp ) {
            if (err) {
              // The file isn't in the repo (perhaps someone deleted it out from under another user), so create one now and add it to the repo
              addFile(paths, workingCopyCleanupCallback);
            } else {
              // Checkout the file and update it
              svn.commands.update(paths.localPathEscaped + paths.fileNameEscaped, { username: req.session.user, password: req.session.pass, force: true }, function( err ) {
                if (err) {
                  clearWorkingCopyPath();
                  sendError(res, err);
                } else {
                  updateFile(paths, workingCopyCleanupCallback);
                }
              });
            }
          });


        }, (err) => {
          if (("" + err).match(/^svn:\sE170000:\sURL.*?doesn't\sexist$/gmi)) {
            // The directory that this file was in has been deleted or renamed
            var output = { success: false, errorCode: 404, errorMsg: 'Directory does not exist.' }
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(output));
          } else {
            sendError(res, err);
          }
        });
        break;

    default:
        return sendError(res, 'Unknown action ' + action);
  }

});

var getRootWorkingCopyPath = function() {
    return path.resolve(__dirname, '../../../svn-working-copy/');
}

var getWorkingCopyPath = function() {
  var folderId = 0;
  var padLen = 4;
  var workingCopyPath =  getRootWorkingCopyPath();

  if (workingCopyPath.substring(-1) != '/') {
    workingCopyPath += '/';
  }

  while (folderId++ < 1000) {
    var folderName = "" + folderId;
    folderName = workingCopyPath + Array(Math.max(0, (padLen+1)-folderName.length)).join('0') + folderName
    if (fs.existsSync(folderName)) {
      continue;
    }

    fs.mkdirSync(folderName);
    if (fs.existsSync(folderName)) {
      return folderName;
    }
  }

  throw 'Unable to create working copy path';
}

/*
 Clear the working copy and checkout a file or directory
*/
var svnCheckout = function(req, res, path, options, successCallback, errorCallback) {
  try {
    path = sanitizePath(path);
  } catch (err) {
    return errorCallback(err);
  }

  var pathInfo = path.split(/^(.*\/)([^\/]+)(\/)?$/g);
  if (!pathInfo || pathInfo.length < 4) {
    return errorCallback('Invalid path '+path);
  }

  var containingDirectory = pathInfo[1];
  var fileName = pathInfo[2];
  var fileNameEscaped = fileName.replace(/ /g, '\\ ');
  var isFile = (pathInfo[3] == '/') ? false : true;

  try {
    var workingCopyPath = getWorkingCopyPath();
  } catch (err) {
    return errorCallback(err, () => { });
  }

  var clearWorkingCopyPath = function() {
    var rootPath = getRootWorkingCopyPath();
    if (rootPath && rootPath.length > 1 && fs.existsSync(workingCopyPath) && workingCopyPath.substring(0, rootPath.length) == rootPath) {
      fs.removeSync(workingCopyPath);
    }
  }

  // Checkout the full path
  var localPath = workingCopyPath + path;
  var localPathEscaped = localPath.replace(/ /g, '\\ ');
  var remotePath = req.app.get('config').svnPath+encodeURIComponent(path);

  if (isFile || (options && options.containingDirectory == true)) {
    // Checkout the containing directory instead of the full path
    localPath = workingCopyPath + containingDirectory;// + '/';
    localPathEscaped = localPath.replace(/ /g, '\\ ');
    remotePath = req.app.get('config').svnPath+encodeURIComponent(containingDirectory)+'/';
  }

    svn.commands.checkout(remotePath, localPathEscaped, { username: req.session.user, password: req.session.pass, depth: 'empty', force: true }, function( err ) {
      if (err) {
        errorCallback(err);
        clearWorkingCopyPath();
      } else {
        if (isFile && (!options || options.containingDirectory == false)) {
          // Checkout the file
          localPath += fileName;
          localPathEscaped += fileName.replace(/ /g, '\\ ');
          remotePath += encodeURIComponent(fileName);

          try {
            fs.unlinkSync(localPath);
          } catch (e) {

          }

          svn.commands.update(localPathEscaped, { username: req.session.user, password: req.session.pass, force: true }, function( err ) {
            if (err) {
              clearWorkingCopyPath();
              sendError(res, err);
            } else {
              successCallback({localPath: localPath, localPathEscaped: localPathEscaped, remotePath: remotePath, containingDirectory: containingDirectory, fileName: fileName, fileNameEscaped: fileNameEscaped, isFile: isFile}, clearWorkingCopyPath);
            }
          });
        } else {
          // We're done
          successCallback({localPath: localPath, localPathEscaped: localPathEscaped, remotePath: remotePath, containingDirectory: containingDirectory, fileName: fileName, fileNameEscaped: fileNameEscaped, isFile: isFile}, clearWorkingCopyPath);
        }
      }
    });

}


module.exports = router;
