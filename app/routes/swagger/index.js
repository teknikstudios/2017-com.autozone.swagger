var express = require('express');
var fs = require('fs');
var path = require('path');
var app = express();

app.enable('strict routing');

var router = express.Router({
    caseSensitive: app.get('case sensitive routing'),
    strict       : app.get('strict routing')
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/swagger/editor/');
});

router.get('/editor/dist/swagger-editor-standalone-preset.js', function(req, res, next) {
  let contents = fs.readFileSync(path.resolve(__dirname, '../../public/lib/swagger-editor/3.1.2/dist/swagger-editor-standalone-preset.js'), 'utf8');
  contents = contents.replace(/http:\/\/localhost:3000/g, (req.app.get('config').useSsl ? 'https://' : 'http://') + req.app.get('config').hostname);
  contents = contents.replace(/http:\/\/127.0.0.1:3000/g, (req.app.get('config').useSsl ? 'https://' : 'http://') + req.app.get('config').hostname);
  res.send(contents);
});

router.get('/editor/dist/swagger-editor-bundle.js', function(req, res, next) {
  let contents = fs.readFileSync(path.resolve(__dirname, '../../public/lib/swagger-editor/3.1.2/dist/swagger-editor-bundle.js'), 'utf8');
  contents = contents.replace(/http:\/\/localhost:3000/g, (req.app.get('config').useSsl ? 'https://' : 'http://') + req.app.get('config').hostname);
  contents = contents.replace(/http:\/\/127.0.0.1:3000/g, (req.app.get('config').useSsl ? 'https://' : 'http://') + req.app.get('config').hostname);
  res.send(contents);
});

router.get('/editor/', function(req, res, next) {
  let contents = fs.readFileSync(path.resolve(__dirname, '../../public/lib/swagger-editor/3.1.2/index.html'), 'utf8');
  contents = contents.replace(/http:\/\/localhost:3000/g, (req.app.get('config').useSsl ? 'https://' : 'http://') + req.app.get('config').hostname);
  contents = contents.replace(/http:\/\/127.0.0.1:3000/g, (req.app.get('config').useSsl ? 'https://' : 'http://') + req.app.get('config').hostname);
  res.send(contents);
});

router.get(/^\/viewer\/((snippets)|(specs))\/(.(?!\.(js|png|jpg|css)$))*$/, function(req, res, next) {
  let url = req.url.replace(/^\/viewer\/((snippets)|(specs))\//g, (req.app.get('config').useSsl ? 'https://' : 'http://') +req.app.get('config').hostname+'/svn/$1/');
  let contents = fs.readFileSync(path.resolve(__dirname, '../../public/lib/swagger-ui/3.4.0/index.html'), 'utf8');
  contents = contents.replace(/\.\//g, '/swagger/viewer/');
  contents = contents.replace(/\{swagger-spec-url\}/g, url);

  res.send(contents);
});

router.get('/viewer/', function(req, res, next) {
  res.redirect('/swagger/editor/');
});

module.exports = router;
