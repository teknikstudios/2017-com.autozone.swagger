var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var svn = require('node-svn-ultimate');

router.use(bodyParser.json({limit:"5mb"}));
router.use(bodyParser.urlencoded({limit:"5mb", extended:false}));

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/swagger/editor/');
});

/* GET login page . */
router.get('/login', function(req, res, next) {
  res.render(__dirname + '/login', { hostname: 'test', title: 'Login', id: 'login' });
});

/* Authentication . */
router.post('/login', function(req, res, next) {
  svn.commands.list(req.app.get('config').svnPath, { username: req.body.username, password: req.body.password }, function(err, files) {
    res.setHeader('Content-Type', 'application/json');
    req.session.authorized = true;
    req.session.user = req.body.username;
    req.session.pass = req.body.password;
    res.send(JSON.stringify({success:(err?false:true)}));
  });
});

/* GET logout page . */
router.get('/logout', function(req, res, next) {
  res.render(__dirname + '/logout', { title: 'Logout', id: 'logout' });
});


module.exports = router;
