var express = require('express');
var fs = require('fs-extra');
var bodyParser = require('body-parser');
var path = require('path');
var svn = require('node-svn-ultimate');
var app = express();
var Ajv = require('ajv');
var fetch = require("fetch").fetchUrl;

var ajv = new Ajv({
  meta: false, // optional, to prevent adding draft-06 meta-schema
  extendRefs: true, // optional, current default is to 'fail', spec behaviour is to 'ignore'
  unknownFormats: 'ignore'  // optional, current default is true (fail)
});

app.enable('strict routing');

var router = express.Router({
    caseSensitive: app.get('case sensitive routing'),
    strict       : app.get('strict routing')
});

router.use(bodyParser.json({limit:"5mb"}));
router.use(bodyParser.urlencoded({limit:"5mb",extended:true}));

/*
 sendError
*/
var sendError = function(res, err) {
  //console.log(err);
  res.status(500).send({ success: false, error: err })
  return false;
}

/*
 Validate a json spec from text
*/
router.post('/validatefromtext', function(req, res, next) {
  if (!req.body || !req.body.json || !req.body.schemaText) {
    res.setHeader('Content-Type', 'application/json');
    res.status(500).send({success:false, errors: 'Invalid request'});
    return;
  }

  let jsonToValidate = '';
  try {
    jsonToValidate = JSON.parse(req.body.json);
  } catch (err) {
    res.setHeader('Content-Type', 'application/json');
    res.status(500).send({success:false, errors: 'Your document is not valid json. ' + err});
    return;
  }

  let schema = '';
  try {
    schema = JSON.parse(req.body.schemaText);
  } catch (err) {
    res.setHeader('Content-Type', 'application/json');
    res.status(500).send({success:false, errors: 'Your JSON schema is not a valid JSON document. ' + err});
    return;
  }

  try {
    var metaSchema = require('ajv/lib/refs/json-schema-draft-04.json');
    try {
      ajv.addMetaSchema(metaSchema);
      ajv._opts.defaultMeta = metaSchema.id;

      // optional, using unversioned URI is out of spec, see https://github.com/json-schema-org/json-schema-spec/issues/216
      ajv._refs['http://json-schema.org/schema'] = 'http://json-schema.org/draft-04/schema';

      // Optionally you can also disable keywords defined in draft-06
      ajv.removeKeyword('propertyNames');
      ajv.removeKeyword('contains');
      ajv.removeKeyword('const');
    } catch (err) {

    }

    let valid = ajv.validate(schema, jsonToValidate);
    let resp = {success:true};

    if (!valid) {
      res.setHeader('Content-Type', 'application/json');
      resp = {success:false, errors: ajv.errors};
    }

    res.setHeader('Content-Type', 'application/json');
    res.send(resp)
  } catch (err) {
    res.setHeader('Content-Type', 'application/json');
    res.status(500).send({success:false, errors: 'There was a problem validating your JSON schema. ' + err});
    return;
  }
});

/*
 Validate a json spec from a url
*/
router.post('/validatefromurl', function(req, res, next) {
  if (!req.body || !req.body.json || !req.body.schemaUrl) {
    res.setHeader('Content-Type', 'application/json');
    res.status(500).send({success:false, errors: 'Invalid request'});
    return;
  }

  let jsonToValidate = '';
  try {
    jsonToValidate = JSON.parse(req.body.json);
  } catch (err) {
    res.setHeader('Content-Type', 'application/json');
    res.status(500).send({success:false, errors: 'Your document is not valid json. ' + err});
    return;
  }

  fetch(req.body.schemaUrl, function(error, meta, body){
    if (error) {
      res.setHeader('Content-Type', 'application/json');
      res.send({success:false, errors: "" + error});
      return;
    }

    let schema = '';
    try {
      schema = JSON.parse(body.toString());
    } catch (err) {
      res.setHeader('Content-Type', 'application/json');
      res.send({success:false, errors: 'Error fetching schema from url ' + req.body.schemaUrl + '. ' + err});
      return;
    }

    let resp = {success:true};

    try {
      var metaSchema = require('ajv/lib/refs/json-schema-draft-04.json');
      try {
        ajv.addMetaSchema(metaSchema);
        ajv._opts.defaultMeta = metaSchema.id;

        // optional, using unversioned URI is out of spec, see https://github.com/json-schema-org/json-schema-spec/issues/216
        ajv._refs['http://json-schema.org/schema'] = 'http://json-schema.org/draft-04/schema';

        // Optionally you can also disable keywords defined in draft-06
        ajv.removeKeyword('propertyNames');
        ajv.removeKeyword('contains');
        ajv.removeKeyword('const');
      } catch (err) {

      }

      let valid = ajv.validate(schema, jsonToValidate);

      if (!valid) {
        resp = {success:false, errors: ajv.errors};
      }
    } catch (err) {
      res.setHeader('Content-Type', 'application/json');
      res.send({success:false, errors: "" + err});
      return;
    }

    res.setHeader('Content-Type', 'application/json');
    res.send(resp)
  });
});


module.exports = router;
