### AutoZone Swagger

There are 3 components to AutoZone Swagger;

- Swagger UI
- Swagger Editor
- Web App

Swagger UI and Swagger Editor are built into static HTML files using ```npm```

Web App is a node.js application that has some logic for UAC and for communicating with SVN. It wraps that logic around the static HTML Swagger UI/Editor build files.

---

##### Swagger UI

Clone or download the Swagger UI project from <https://github.com/teknikstudios/swagger-ui>. _This project is forked from v3.4.0 of <https://github.com/swagger-api/swagger-ui>._

From the project directory, execute:
```bash
npm run build
```

After the build is completed, static files will be output to the ```~/dist/``` folder. Copy the contents of the ```~/dist/``` folder to the Web App's ```~/app/public/lib/swagger-ui/3.4.0/dist/``` folder.

---

##### Swagger Editor

Clone or download the Swagger Editor project from <https://github.com/teknikstudios/swagger-editor>. _This project is forked from v3.1.2 of <https://github.com/swagger-api/swagger-editor>._

From the project directory, execute:
```bash
npm run build
```

After the build is completed, static files will be output to the ```~/dist/``` folder. Copy the contents of the ```~/dist/``` folder to the Web App's ```~/app/public/lib/swagger-editor/3.1.2/dist/``` folder.


---

##### Web App

Clone or download the Web App project from <https://bitbucket.org/teknikstudios/2017-com.autozone.swagger>

__Requirements__

- NodeJS
- Subversion client


__Subversion__

User authentication and file management is handled via calls to an SVN server. Therefore, the app requires a local SVN command line client to be visible in your system's PATH environment variable.

User accounts must be setup on your subversion server in order to log into the app, as authentication is handled via calls to the svn client.


__Configuration__

There are 3 configurations available for this project; ```local```, ```dev``` and ```dist```.

- The ```local``` configuration is used when developing the app on your local machine.
- The ```local``` configuration is used when testing the app on a development server.
- The ```dist``` configuration is used on the production server.

Each configuration requires a corresponding json configuration file named ```config-local.json```, ```config-dev.json```, and ```config-dist.json``` respectively. These files should be placed in the ```~/app/``` folder of the project.

The configuration file format is as follows:

```json
{
  "env": "dev",
  "hostname": "localhost:3001",
  "port": 3001,
  "useSsl": true,
  "useSslCookies": true,
  "cookieDuration": 86400000,
  "cookieActiveDuration": 3600000,
  "cookieIsEphemeral": true,
  "hostip": "0.0.0.0",
  "svnPath": "https://svn.riouxsvn.com/autozone-swag/"
}
```

```env``` may have a value of either ```dev``` or ```dist```

If ```port``` is not ```80``` or ```443```, you will need to include the port number at the end of ```hostname``` as shown above.

If ```useSslCookies``` is true, the cookie will only be sent over SSL. use key 'secureProxy' instead if you handle SSL not in your node process

```cookieDuration``` is the duration that the session will stay valid in milliseconds

If the cookie's expiresIn < ```cookieActiveDuration```, the session will be extended by ```cookieActiveDuration``` milliseconds

If ```cookieIsEphemeral``` is true, the cookie expires when the browser closes


__Running__

From the application directory, execute ```npm run start``` to launch the application using the ```dist``` configuration file.

- ```npm run localwatch``` launches using the ```local``` configuration file and watches the filesystem for changes to files
- ```npm run localstart``` launches using the ```local``` configration file and does not watch the filesystem for changes to files
- ```npm run devwatch``` launches using the ```dev``` configration file and watches the filesystem for changes to files
- ```npm run devstart``` launches using the ```dev``` configration file and does not watch the filesystem for changes to files
- ```npm run start``` launches using the ```dist``` configration file and does not watch the filesystem for changes to files
